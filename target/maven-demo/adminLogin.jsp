<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/11/3
  Time: 15:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>管理员登录</title>

    <link rel="stylesheet" type="text/css" href="css/adminLogin.css">
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $("input[type=button]").click(function () {
                var name = $("input[name=mname]").val();
                var pwd = $("input[name=mpassword]").val();
                if (name == "" ) {
                    $("#error").val("用户名不能为空");
                } else if (pwd == "" ) {
                    $("#error").val("密码不能为空");
                } else {
                    $.ajax({
                        url: "manager/login",
                        type: "post",
                        data: {"name": name, "pwd": pwd},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                window.location.href = "details2.jsp";
                            } else {
                                $("#error").val("用户名或密码错误");
                            }
                        }
                    })
                }
            });

            $(document).keydown(function (event) {
                if (event.keyCode == "13") {
                    $("input[type=button]").click();
                }
            });
        })
    </script>




    <style type="text/css">
 body{
     cursor: pointer;
 }
 #woaisiw{
     height: 600px;
     width: 1200px;
     margin:  0 auto;
     border: 1px solid rgb(209,218,225);

     background-image: url(./image/aisisi.png);

 }
    </style>

</head>

<body>
<div id="zong_112">
    <div class="av9_"><span>胡萝卜网首页</span></div>
    <div class="right09_"><a href="">登录</a>&nbsp;&nbsp;<a href="">注册</a>&nbsp;&nbsp;<a href="">管理员登陆</a></div>
</div>
<div id="top3o_">
    <div class="tleft">
        <a class="tf" href=""><img src="image/5.jpg" alt=""></a>
    </div>
    <div class="tmiddle">
        <input class="tm" type="text" placeholder="请输入路线名称">
        <div class="tmr">
            <a  class="ai_" href="">搜索</a>
        </div>
    </div>
    <div class="tright">
        <img class="img_" src="image/hot_tel.jpg" alt="">
        <p class="topword">客服热线:9:00-6:00</p>
        <p class="downword">400-618-9090</p>
    </div>
</div>

<div id="titles0_">
    <div class="ad124">
        首页
    </div>
    <div class="ad134">国内游</div>
    <div class="ad144">国外游</div>
    <div class="ad154">我的收藏</div>
</div>
<div class="kongbai"></div>
<div id="woaisiw">
    <div id="mainai">
        <div class="toumi">管理员登录</div>
        <div class="zhongmi">
            <input class="nisa" type="text" name="mname" placeholder="请输入账号"><br>
            <input class="nisa" type="password" name="mpassword" placeholder="请输入密码"><br>
            <input id="error" class="nisayi"  type="text">
        </div>
        <div class="weimi">
            <input class="weiaiw" type="button" name=""  value="登录" />
        </div>
    </div>
</div>
<div style="height:80px"></div>
<div class="adw8">
    <ul>
        <li><a href="">关于我们</a></li>
        <li><a href="">About us</a></li>
        <li><a href="">Investor Relations</a></li>
        <li><a href="">媒体报道</a></li>
        <li><a href="">品牌招商</a></li>
        <li><a href="">隐私条款</a></li>
        <li><a href="">胡萝卜诚聘</a></li>
        <li><a href="">联系我们</a></li>
    </ul>
</div>
<div class="footmain">
    <img src="image/2ac.png" alt="">

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
</body>
</html>
