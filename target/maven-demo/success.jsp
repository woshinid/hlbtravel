<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
	<meta charset="UTF-8">
	<title>支付成功</title>
	<!--导入angularJS文件-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/common.css">

</head>

<body>
<div style="width: 100%;">
	<div style="width: 50%; margin:30px auto;">
		<div style="text-align: center;margin-left:240px;">
			<img src="images/jump_1.jpg" style="width: 180px;" />
			<br />
		</div>
		<div style="text-align: center;">
			<font style="font-size: 50px;color: #7AC52B;">支付成功！</font>
			<br />
		</div>

		<div style="text-align: center;">
			${msg }
			<br /> 下单成功!
			<br />
				<a href="orders.jsp" style="background-color: white;font-size: 14px;text-decoration: underline;color: blue;">支付完成，点击查看订单</a>
		</div>
	</div>
</div>
<hr />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
<div class="company">
	<p style="background: white;">江苏胡萝卜文化旅游股份有限公司 版权所有Copyright 2006-2019, All Rights Reserved 苏ICP备16007882</p>
</div>
</body>
</html>