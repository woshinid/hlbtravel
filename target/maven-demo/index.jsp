
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>旅游网</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
</head>









<body>
<!--引入头部-->
<div id="header"></div>
<!-- banner start-->
<section id="banner">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="2000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="images/banner_1.jpg" alt="">
            </div>
            <div class="item">
                <img src="images/banner_2.jpg" alt="">
            </div>
            <div class="item">
                <img src="images/banner_3.jpg" alt="">
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!-- banner end-->
<!-- 旅游 start-->
<section id="content">
    <!-- start-->
    <section class="hemai_jx">
        <div class="jx_top">
            <div class="jx_tit">
                <img src="images/icon_5.jpg" alt="">
                <span>精选</span>
            </div>
            <!-- Nav tabs -->
            <ul class="jx_tabs" role="tablist">

                <li role="presentation" class="active">
                    <span></span>
                    <a href="#popularity" aria-controls="popularity" role="tab" data-toggle="tab">人气旅游</a>
                </li>
                <li role="presentation">
                    <span></span>
                    <a href="#newest" aria-controls="newest" role="tab" data-toggle="tab">最新旅游</a>
                </li>
                <li role="presentation">
                    <span></span>
                    <a href="#theme" aria-controls="theme" role="tab" data-toggle="tab">主题旅游</a>
                </li>
            </ul>
        </div>
        <div class="jx_content">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="popularity">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=7">
                                <img src="images/guonei_tu/suzhou.jpg" alt="">
                                <div class="has_border">
                                    <h3>苏州+乌镇+杭州3日2晚跟团游【国内】国内纯玩不进店 精选4个5A级景区 真正深度漫游三城 360度玩转乌镇日夜景 苏州双5A园林 赠西湖游船 三潭映月 远观雷峰塔 乌镇多种住宿任选</h3>
                                    <div class="price">网付价<em>￥</em><strong>698</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=8">
                                <img src="images/guonei_tu/shanghai.jpg" alt="">
                                <div class="has_border">
                                    <h3>上海+乌镇+杭州3日2晚跟团游【国内】纯玩不进店，1天1座城，小华东经典连线，赠游船环游西湖美景，深度漫游乌镇 ，赏乌镇西栅日夜全景，乌镇住宿多种可选，上海1日自由活动，华东一次玩个够！</h3>
                                    <div class="price">网付价<em>￥</em><strong>904</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="javascript:;">
                                <img src="images/guonei_tu/huangshan.jpg" alt="">
                                <div class="has_border">
                                    <h3>黄山风景区3日2晚跟团游【国内】A线纯黄山 B线黄山+宏村 C线黄山+香溪漂流 D线黄山+九龙瀑 E线黄山+翡翠谷 点评返现30元/单 专业导游讲解 可升级住宿 1人成团&A、C线赠登山三宝 周边热卖</h3>
                                    <div class="price">网付价<em>￥</em><strong>556</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=57">
                                <img src="images/guonei_tu/shamo-7.jpg" alt="">
                                <div class="has_border">
                                    <h3>兰州+张掖丹霞+敦煌莫高窟+吐鲁番+新疆沙漠双飞7日游 丝绸之路，千年莫高文化，沙漠越野冲浪，优选住宿，24H接送机</h3>
                                    <div class="price">网付价<em>￥</em><strong>4754</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="newest">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=10">
                                <img src="images/guonei_tu/jiangxi.jpg" alt="">
                                <div class="has_border">
                                    <h3>江西婺源+三清山3日2晚跟团游【国内】16人精致小团 A线丛溪+希尔顿 开元 B线2晚希尔顿 C线2晚开元 D线鑫邦开元+希尔顿 晒秋篁岭赠索道 道教仙山三清奇景 </h3>
                                    <div class="price">网付价<em>￥</em><strong>1777</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=11">
                                <img src="images/guonei_tu/putuoshan.jpg" alt="">
                                <div class="has_border">
                                    <h3>普陀山+宁波3日2晚跟团游【国内】门票全含0购物 第3天行程可选 A线游玩溪口老街B线穿越雪窦山 宁波住宿可补差价升级宿五星酒店</h3>
                                    <div class="price">网付价<em>￥</em><strong>727</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=12">
                                <img src="images/guonei_tu/hongcun.jpg" alt="">
                                <div class="has_border">
                                    <h3>宏村+西递2日1晚跟团游【国内】A线宿金陵饭店旗下四星级酒店 可特价升级含《宏村阿菊》演出 B线宿西递旁特色王府酒店 玩转徽派古村落</h3>
                                    <div class="price">网付价<em>￥</em><strong>377</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=13">
                                <img src="images/guonei_tu/hengdian.jpg" alt="">
                                <div class="has_border">
                                    <h3>横店影视城2日跟团游【国内】含梦幻谷+影视城4大经典景区+1早3正餐</h3>
                                    <div class="price">网付价<em>￥</em><strong>600</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="theme">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=37">
                                <img src="images/guowai_tu/yiselie-6.jpeg" alt="">
                                <div class="has_border">
                                    <h3>以色列约旦10日7晚游 【国外】全国联运/车载WiFi/拼住无忧/死海阿卡巴升级五星/走遍五大世界遗产/佩特拉古城/约旦河洗礼/月亮之谷瓦地伦</h3>
                                    <div class="price">网付价<em>￥</em><strong> 10964 </strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=18">
                                <img src="images/guowai_tu/taiguo.jpg" alt="">
                                <div class="has_border">
                                    <h3>泰国曼谷+芭堤雅6日5晚跟团游【国外】AB·5晚黄金地段钻5·日落悬崖餐厅·射击馆·快艇沙美·双网红夜市+海鲜自选·21航站楼【CD·8人VIP团·可选万豪或茉莉·6人可免费独立】E·机票自选</h3>
                                    <div class="price">网付价<em>￥</em><strong>2999</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=53">
                                <img src="images/guonei_tu/ss-3.jpg" alt="">
                                <div class="has_border">
                                    <h3>桂林+龙脊梯田+大漓江+阳朔双飞5日 20人一价全含/缆车上梯田/刘三姐/四星船/220高餐标/高标酒店/指定1晚大公馆/儿童含早/专车接送机/出游乐无忧 </h3>
                                    <div class="price">网付价<em>￥</em><strong>5577</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="route_detail.jsp?id=45">
                                <img src="images/guowai_tu/jiujinshan-14.jpg" alt="">
                                <div class="has_border">
                                    <h3>美国西海岸-旧金山-拉斯-洛杉矶机票+当地13日半自助游 【国外】领略莫哈韦特有的壮丽和独特的地貌和繁华都市中难觅的宁静;魅力锡安:溪水潺潺，鸟鸣阵阵</h3>
                                    <div class="price">网付价<em>￥</em><strong>16794</strong><em>起</em></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- 胡萝卜精选end-->
    <!-- 国内游 start-->
    <section class="hemai_jx">
        <div class="jx_top">
            <div class="jx_tit">
                <img src="images/icon_6.jpg" alt="">
                <span>国内游</span>
            </div>
        </div>
        <div class="heima_gn">
            <div class="guonei_l">
                <a href="https://www.w3cschool.cn/">
                    <img src="images/guonei_1.jpg" alt="">
                </a>
            </div>
            <div class="guone_r">
                <div class="row">
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=51">
                            <img src="images/guonei_tu/lasa-1.jpg" alt="">
                            <div class="has_border">
                                <h3>拉萨-林芝-大峡谷-巴松措-布达拉宫-大昭寺双飞7日游0自费，升级1晚洲际，赏鲁朗田园风光，挂经幡祈福，品特色石锅鸡 </h3>
                                <div class="price">网付价<em>￥</em><strong>7263</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=52">
                            <img src="images/guonei_tu/hulun-2.jpeg" alt="">
                            <div class="has_border">
                                <h3>呼伦贝尔-冷极村-驯鹿部落-根河湿地-满洲里双飞双卧5日游 这个冬天告别平凡,10人小团,冬日草原/不冻河/雾凇/泼水成冰/中国Z冷,门票全含,随走随停</h3>
                                <div class="price">网付价<em>￥</em><strong>2720</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=51">
                            <img src="images/guonei_tu/ss-3.jpg" alt="">
                            <div class="has_border">
                                <h3>桂林+龙脊梯田+大漓江+阳朔双飞5日 缆车上梯田/刘三姐/四星船/220高餐标/高标酒店/指定1晚大公馆/儿童含早/专车接送机/出游乐无忧</h3>
                                <div class="price">网付价<em>￥</em><strong>2920</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=54">
                            <img src="images/guonei_tu/lianl-4.jpg" alt="">
                            <div class="has_border">
                                <h3>厦门-鼓浪屿-云水谣土楼双飞5日游3晚核心商圈国际五星酒店+1晚鼓浪屿，探秘云水谣+赏古镇风光，打卡日光岩+闲逛醉美鼓浪屿，胡里山炮台+南普陀寺 </h3>
                                <div class="price">网付价<em>￥</em><strong>1960</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=56">
                            <img src="images/guonei_tu/huyang-6.jpg" alt="">
                            <div class="has_border">
                                <h3>新疆+胡杨林+库车+库尔勒3飞8日游 疆内直飞省车程，南疆深度环线/探秘喀什老城/穿越沙漠公路，享特色美食/保证拼房</h3>
                                <div class="price">网付价<em>￥</em><strong>3641</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=58">
                            <img src="images/guonei_tu/xining-8.jpg" alt="">
                            <div class="has_border">
                                <h3>西宁-青海湖-茶卡盐湖-德令哈-敦煌-嘉峪关-张掖双飞6日游 上班族力荐，0购物深度游不耗时/兰新高铁替代750km大巴车程，西北人气景点，鸣沙山日落</h3>
                                <div class="price">网付价<em>￥</em><strong>3478</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- 国内游 end-->
    <!-- 境外游 start-->
    <section class="hemai_jx">
        <div class="jx_top">
            <div class="jx_tit">
                <img src="images/icon_7.jpg" alt="">
                <span>境外游</span>
            </div>
        </div>
        <div class="heima_gn">
            <div class="guonei_l">
                <a href="https://movie.douban.com/">
                    <img src="images/jiangwai_1.jpg" alt="">
                </a>
            </div>
            <div class="guone_r">
                <div class="row">
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=35">
                            <img src="images/guowai_tu/dibai-4.jpg" alt="">
                            <div class="has_border">
                                <h3>迪拜+阿布扎比+沙迦4晚6日游 南京起止，国泰航空非夜航，含税金含司导服务费，3国联游，全程五星酒店，水上的士，黄金香料市场</h3>
                                <div class="price">网付价<em>￥</em><strong>4578</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=42">
                            <img src="images/guowai_tu/jieke-11.jpg" alt="">
                            <div class="has_border">
                                <h3>奥地利+捷克+匈牙利11日游 自营南京录指纹,25/30人,超4000人,4星,温泉酒店,湖区酒店,维也纳音乐会,美泉宫,CK小镇,哈尔施塔特,艺术史博物馆 </h3>
                                <div class="price">网付价<em>￥</em><strong>13999 </strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=42">
                            <img src="images/guowai_tu/xinxilan-35.jpg" alt="">
                            <div class="has_border">
                                <h3>新西兰-南北岛冰川14日游 真纯玩不进店/冰湖库克山萤火虫温泉/峡湾缆车观星/霍比特村或陶波瀑布/A高山火车农庄/B全程含餐/C秋季凯库拉海岸火车 </h3>
                                <div class="price">网付价<em>￥</em><strong>25899</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=45">
                            <img src="images/guowai_tu/jiujinshan-14.jpg" alt="">
                            <div class="has_border">
                                <h3>美国西海岸-旧金山-拉斯-洛杉矶机票+当地13日半自助游 领略莫哈韦特有的壮丽和独特的地貌和繁华都市中难觅的宁静;
                                    魅力锡安:溪水潺潺，鸟鸣阵阵，仿佛是一片由自然构成的画廊，一步一景，百看不厌。名校伯克利，17里湾，优胜美地，联运，直飞</h3>
                                <div class="price">网付价<em>￥</em><strong>889</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=37">
                            <img src="images/guowai_tu/yiselie-6.jpeg" alt="">
                            <div class="has_border">
                                <h3>以色列约旦10日7晚游 全国联运/车载WiFi/拼住无忧/死海阿卡巴升级五星/走遍五大世界遗产/佩特拉古城/约旦河洗礼/月亮之谷瓦地伦</h3>
                                <div class="price">网付价<em>￥</em><strong>4299</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="route_detail.jsp?id=17">
                            <img src="images/guowai_tu/xinjiapo-17.jpg" alt="">
                            <div class="has_border">
                                <h3>新加坡花园城市滨海湾 5日经典游 畅游鱼尾狮公园、滨海湾花园等动感市区景色，在莱佛士酒店尝一杯新加坡司令，漫步牛车水、小印度及甘榜格南感受复古的韵味，奔向乌节路尽情购物血拼，在圣淘沙岛的环球影城水雾地球前合影留念，尽享游乐设施带来的兴奋欢乐</h3>
                                <div class="price">网付价<em>￥</em><strong>4530</strong><em>起</em></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- 境外游 end-->
</section>
<!-- 旅游 end-->
<!--导入底部-->
<div id="footer"></div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
<script>
    $(function () {
        $.getJSON("user/testCookie",function () {
                window.location.href = "index.jsp";
        })
    })
</script>
</body>
</html>