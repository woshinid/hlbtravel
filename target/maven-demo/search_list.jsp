<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/29
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>胡萝卜旅游-搜索</title>
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <script src="https://cdn.bootcss.com/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/search.css">
    <script src="js/jquery-3.3.1.js"></script>

    <style>
        .tu_size{
            height: 169px;
            width: 299px;
        }

    </style>


    <script>

        $(function () {
            <%
                String details = request.getParameter("details");

            %>

        })

        var pn = 1;
        var ps = 10;
        $(function(){
            query(pn, ps);
        });


        function query(pn, ps) {

            $.getJSON("travel/search",{"pn":pn, "ps":ps,"details":"<%=details %>"}, function (data) {
                if (data.list!=null) {
                    var obj = eval(data);
                    var str = "";
                    $(obj.list).each(function () {
                        str += "<li>" +
                            "<div class='img' class='tu_size'>" + "<img src='" + this.picture + "' alt=''></div>" +
                            "<div class='text1'>" +
                            "<p>" + this.details + "</p>" +
                            "<br/>" +
                            "<p>" + this.discount + "</p>" +
                            "</div>" +
                            "<div class='price'>" +
                            "<p class='price_num'>" +
                            "<span>&yen;</span>" +
                            "<span>" + this.price + "</span>" +
                            "<span>起</span>" +
                            "</p>" +
                            "<p><a href='route_detail.jsp?id=" + this.id + "'>查看详情</a></p>" +
                            "</div>" +
                            "</li>";
                    })
                    $(".tbody").empty();
                    $(".tbody").append(str);
                }else if (data.list == null){

                  alert(不能为空);
                }
                var page = "";
                page += "<ul class=\"pagination\">" +
                    "<li><a href=\"javascript:void(0)\" onclick='query(1,"+ps+")'>首页</a></li>";
                if(obj.hasPreviousPage){
                    page += "<li>" +
                        "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"query("+(obj.pageNum-1)+","+ps+")\">" +
                        "<span aria-hidden=\"true\">&laquo;</span>" +
                        "</a>" +
                        "</li>";
                }

                $(obj.navigatepageNums).each(function () {
                    if(obj.pageNum==this){
                        page += "<li class='active'><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                    }else{
                        page += "<li><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                    }

                })

                if(obj.hasNextPage){
                    page += "<li>\n" +
                        "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='query("+(obj.pageNum+1)+","+ps+")'>" +
                        "<span aria-hidden=\"true\">&raquo;</span>" +
                        "</a>" +
                        "</li>";
                }
                page += "<li><a href=\"javascript:void(0)\" onclick='query("+obj.pages+","+ps+")'>未页</a></li>" +
                    "</ul>";
                $("nav").empty();
                $("nav").append(page);

            });
        }
    </script>


</head>


<body>
<!--引入头部-->
<div id="header"></div>
<div class="page_one">
    <div class="contant">
        <div class="crumbs">
            <img src="images/search.png" alt="">
            <p>胡萝卜旅行><span>搜索结果</span></p>
        </div>
        <div class="xinxi clearfix">
            <div class="left">
                <div class="header">
                    <span>商品信息</span>
                    <span class="jg">价格</span>
                </div>
                <ul class="tbody">

                </ul>

                <div class="pageNum">
                    <nav aria-label="Page navigation">




                    </nav>
                </div>
            </div>
        </div>

<!--引入头部-->
<div id="footer"></div>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
</body>

</html>