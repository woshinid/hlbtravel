<%--
  Created by IntelliJ IDEA.
  User: 19245
  Date: 2019/11/4
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="css/personal.css" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/Order.css" />
    <style>
        .hlong *{display:inline-block;vertical-align:middle}
    </style>
</head>

<body>
<!--引入头部-->x`
<div id="header"></div>

<!--个人资料-->
<div style="width:100% ; height: 550px;background: url(images/bgbg1.png);background-size: 100% 100%;">
    <ul class="sort">
        <li>
            <div style="height: 18px;"></div>
            <div class="navigation" style="box-shadow: 0px 0px 20px darkgrey;">
                <br />
                <div class="perinfo">
                    <div id="uploadify-button" class="uploadify-button " style="height: 80px; line-height: 80px; width: 80px;">
                        <img  src="images/huluobo.png" style="width: 80px;height: 80px;margin-bottom:  5px;border-radius: 40px;margin-left: 33px;">
                    </div>
                    <a><img  src="images/shou1.png" style="width: 30px;height:30px;margin-bottom: 15px;border-radius: 40px;margin:auto;margin-top: 25px;"/></a>
                    <button id="menuA" href="#" onclick="changeA()"><div class="hlong"><img src="images/small1.png"><span style="color: #5e5e5e">个人信息</span></div></button>
                    <button id="menuB" href="#" onclick="changeB()"><div class="hlong"><img src="images/small2.png"><span style="color: #5e5e5e">我的订单</span></div></button>
                    <button id="menuC" href="#" onclick="changeC()"><div class="hlong"><img src="images/small3.png"><span style="color: #5e5e5e">我的收藏</span></div></button>

                </div>
            </div>
        </li>
        <li>
            <div style="height: 18px;"></div>
            <div class="data_box" style="box-shadow: 0px 0px 20px darkgrey;background-color: white;">
                <div style="height: 25px;"></div>
                <div class="show_data" align="center" >
                    <div class="tbody2" align="center" style="font-size: 20px;line-height: 30px;">

                    <!--插入个人信息-->
                    </div>
                        <a style="font-size:15px;color: blue;text-decoration:underline;text-align: center" name="jump">[修改个人信息]</a>
                    <a style="font-size:15px;color: blue;text-decoration:underline;text-align: center" name="xiugai">[修改密码]</a>


                    </div>


                </div>
            <div style=" height: 18px;"></div>
        </li>
    </ul>
</div>
<!--导入底部-->
<div id="footer"></div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
<script>
    $(function(){

        $.getJSON("user/list",{"id":"${sessionScope.user.id}"},function(data){

            if (data != null){
                var str = "";
                $(data).each(function(){

                    str +=
                        // "<tr>"+
                        "<tr><td>用户ID："+this.id+"</td></tr>"+
                        "<tr><td>用户名："+this.username+"</td></tr>"+
                        "<tr><td>Email："+this.email+"</td></tr>"+
                        "<tr><td>真实姓名："+this.name+"</td></tr>"+
                        "<tr><td>电话号码："+this.telephone+"</td></tr>"+
                        "<tr><td>性别："+this.sex+"</td></tr>"+
                        "<tr><td>生日："+this.birthday+"</td></tr>"
                        // "</tr>";
                })
                $(".tbody2").append(str);
                $("a[name=jump]").click(function () {
                    window.location.href = "update.jsp?id=${sessionScope.user.id}";
                })
            }
        })

        $("a[name=xiugai]").click(function () {
            window.location.href = "getPassword.jsp";
        })

    })
</script>
<script type="text/javascript">
    function changeA() {
        window.location.href = "personal.jsp";
        document.getElementById("menuA").style.background = '#FFD800';

        document.getElementById("menuB").style.background = '#FFC900';

        document.getElementById("menuC").style.background = '#FFC900';

    }

    function changeB() {
        window.location.href = "orders.jsp";
        document.getElementById("menuB").style.background = '#FFD800';

        document.getElementById("menuA").style.background = '#FFC900';

        document.getElementById("menuC").style.background = '#FFC900';

    }

    function changeC() {
        window.location.href = "myfavorite.jsp";
        document.getElementById("menuC").style.background = '#FFD800';

        document.getElementById("menuA").style.background = '#FFC900';

        document.getElementById("menuB").style.background = '#FFC900';

    }
</script>
</body>
</html>
