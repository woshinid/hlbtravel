<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/29
  Time: 15:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>胡萝卜旅游网-登录</title>
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--导入angularJS文件-->
    <%--<script src="js/angular.min.js"></script>--%>
    <!--导入jquery-->
    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">

        $(function () {
            $("button[type=button]").click(function () {
                var username = $("input[name=username]").val();
                var password = $("input[name=password]").val();
                var validationCode = $("#validationCode").val();
                if (username == "") {
                    $("#error").html("用户名不能为空！");
                } else if (password == "") {
                    $("#error").html("密码不能为空！");
                } else if (validationCode == "") {
                    $("#error").html("验证码不能为空！");
                } else {
                    $.ajax({
                        url: "user/login",
                        type: "post",
                        data: {"username": username, "password": password, "validationCode": validationCode},
                        dataType: "json",
                        success: function (data) {
                            if (data==3) {


                                if ($("input[class=checkbox]").is(":checked")){

                                 $.getJSON("user/cookieUser",{"username": username, "password": password},function () {

                                 })

                                }

                                window.location.href = "index.jsp";
                            } else if (data == 1) {
                                $("#error").html("用户名或者密码错误！");
                            } else if (data == 2) {
                                $("#error").html("验证码输入错误 ！");
                            }
                        }
                    })
                }
            });
        })
        $(function () {
            //Enter触发
            $('body').keydown(function () {
                if (event.keyCode == 13) {   //enter键值为13
                    $("button[type=button]").click();
                }
            });
        });
    </script>
</head>

<body>
<!--引入头部-->
<%--<div id="header"></div>--%>
<jsp:include page="header.jsp"></jsp:include>
<!-- 头部 end -->
<section id="login_wrap">
    <div class="fullscreen-bg" style="background: url(images/abc.jpg);height: 532px;">

    </div>
    <div class="login-box">
        <div class="title">
            <span>请登录您的账户</span>
        </div>
        <div class="login_inner">

            <!--登录错误提示消息-->

            <form id="form1" action="" method="post" accept-charset="utf-8">
                <input type="hidden" name="action" value="login"/>
                <input type="text" name="username" placeholder="请输入账号" autocomplete="off" maxlength="14">
                <input type="password" name="password" placeholder="请输入密码" autocomplete="off" maxlength="30">
                <div class="verify">
                    <input type="text" name="validationCode" id="validationCode" placeholder="请输入验证码">
                    <span> <img src="verification_code.jsp" id="picture" onClick="change()" id="picture"></span>
                    <script>
                        function change() {
                            var pic = document.getElementById("picture");
                            var i = Math.random();//目的是使页面不一样
                            pic.src = "verification_code.jsp?id=" + i;
                        }
                    </script>
                </div>
                <div class="submit_btn">
                    <button type="button" id="login_a" style="margin-top:20px;">登录</button>
                    <div class="auto_login">
                        <input type="checkbox" name="" class="checkbox">
                        <span >自动登录</span>
                        <span><a href="getPassword.jsp" style="margin-left: 70px">忘记密码？</a></span>
                    </div>
                </div>

            </form>
            <div id="error" type="text" class="alert alert-danger"></div>
            <div class="reg">没有账户？<a href="register.jsp">立即注册</a></div>
        </div>
    </div>
</section>
<!--引入尾部-->
<%--<div id="footer"></div>--%>
<jsp:include page="footer.jsp"></jsp:include>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<%--<script type="text/javascript" src="js/include.js"></script>--%>
</body>
</html>
