<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2019/10/31
  Time: 12:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
    <base href="<%=basePath%>" >

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>旅游网</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/Order.css">
    <link rel="stylesheet" type="text/css" href="css/personal.css">

    <style type="text/css">
        span{
            color: blue;
        }
        span:hover{
            color: red;
            cursor: pointer;
        }

        .hlong *{display:inline-block;vertical-align:middle}



    </style>

</head>

<body>
<!--引入头部-->
<div id="header"></div>
<meta charset="UTF-8">

<!--订单-->
<div style="width:100% ; height: 900px;background: url(images/bgbg1.png);background-size: 100% 100%;">
    <ul class="sort">
        <li>
            <div style="height: 18px;"></div>
            <div class="navigation" style="box-shadow: 0px 0px 20px darkgrey;">
                <br />
                <div class="perinfo">
                    <div id="uploadify-button" class="uploadify-button " style="height: 80px; line-height: 80px; width: 80px;">
                        <img  src="images/huluobo.png" style="width: 80px;height: 80px;margin-bottom:  5px;border-radius: 40px;margin-left: 33px;">
                    </div>
                    <a><img  src="images/shou1.png" style="width: 30px;height:30px;margin-bottom: 15px;border-radius: 40px;margin:auto;margin-top: 25px;"/></a>
                <button id="menuA" href="#" onclick="changeA()"><div class="hlong"><img src="images/small1.png"><span style="color: #5e5e5e">个人信息</span></div></button>
                <button id="menuB" href="#" onclick="changeB()"><div class="hlong"><img src="images/small2.png"><span style="color: #5e5e5e">我的订单</span></div></button>
                <button id="menuC" href="#" onclick="changeC()"><div class="hlong"><img src="images/small3.png"><span style="color: #5e5e5e">我的收藏</span></div></button>

            </div>
            </div>
        <li>
            <div id="root" class="root-container">
                <ul id="base_wrapper">
                    <li id="base_bd" class="layout-nm">
                        <div id="leftNavWrapper" menutype="0" menuid="order_all">
                            <div id="sideNavCss">
                            </div>
                            <div id="sideNav" class="aside clearfix">
                                <div class="main">
                                    <div class="main-w">

<%--                                        <div class="order_tab_box">--%>
<%--                                            <div class="order_tab">--%>
<%--                                                <a href="#" rel="noopener referrer" class="current" style="background-color: white;">全部订单</a>--%>
<%--                                                <a href="#" rel="noopener referrer" class="current" style="background-color: white;">待支付</a>--%>
<%--                                                <a href="#" rel="noopener referrer" class="current" style="background-color: white;">已付款</a>--%>
<%--                                            </div>--%>
<%--                                            <hr />--%>
<%--                                        </div>--%>

                                        <div style="height: 18px;"></div>
<%--                                        <div class="orders_list" >--%>

<%--                                            <ul class="t_head">--%>
<%--                                                <li class="t_order_con">订单内容</li>--%>
<%--                                            </ul>--%>
<%--                                            <ul class="t_body"></ul>--%>
<%--                                        </div>--%>
<%--                                        <div class="space"></div>--%>
                                        <div class="order_list_empty" style="box-shadow: 0px 0px 20px darkgrey;background-color: white;">
                                            <div class="nmsl">我的订单</div>
                                            <table style="width: 1000px;">
                                                <thead>
                                                <div>
                                                <tr class="nmsl" style="line-height: 50px;">
                                                    <th style="text-align: center;">订单编号</th>
                                                    <th style="text-align: center;">订单时间</th>
                                                    <th style="text-align: center;">订单详情</th>
                                                    <th style="text-align: center;">订单价格</th>
                                                    <th style="text-align: center;">订单状态</th>
                                                    <th style="text-align: center;">订单操作</th>
                                                </tr>
                                                </div>

                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
<%--    <div class="row text-center">--%>
<%--        <nav aria-label="Page navigation">--%>
<%--            <ul class="pagination">--%>
<%--                <li><a href="#">首页</a></li>--%>
<%--                <li>--%>
<%--                    <a href="#" aria-label="Previous">--%>
<%--                        <span aria-hidden="true">&laquo;</span>--%>
<%--                    </a>--%>
<%--                </li>--%>
<%--                <li><a href="#">1</a></li>--%>
<%--                <li><a href="#">2</a></li>--%>
<%--                <li><a href="#">3</a></li>--%>
<%--                <li><a href="#">4</a></li>--%>
<%--                <li><a href="#">5</a></li>--%>
<%--                <li>--%>
<%--                    <a href="#" aria-label="Next">--%>
<%--                        <span aria-hidden="true">&raquo;</span>--%>
<%--                    </a>--%>
<%--                </li>--%>
<%--                <li><a href="#">未页</a></li>--%>
<%--            </ul>--%>
<%--        </nav>--%>
<%--    </div>--%>
                                        <div style="height: 18px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <li>
                </ul>
            </div>
        </li>
    </ul>
</div>
<!--导入底部-->
<div id="footer"></div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<script>

        $(function () {

            $.getJSON("orders/list",{"uid":${user.id}},function (data) {

                if (data != null){
                    var str = "";
                    $(data).each(function(){
                        var  statement = "";
                        if (this.state==1){
                            statement = "未付款";
                            str += "<tr>"+

                                "<td>"+this.id+"</td>"+
                                "<td>"+this.publishdate+"</td>"+
                                "<td>"+this.details+"</td>"+
                                "<td>"+this.price+"元</td>"+
                                "<td>"+statement+"</td>"+
                                "<td align='center'>" +
                                "<span onclick='del("+this.id+", this)'><div class='hlong'><img src='images/delete.png' ></div></span> " +
                                "<span onclick='zhifu("+this.id+")'><div><img src='images/small4.png' ></span></div></td>"+
                                "</tr>";
                        }else if (this.state==2 ){
                            statement = "已付款";
                            str += "<tr>"+

                                "<td>"+this.id+"</td>"+
                                "<td>"+this.publishdate+"</td>"+
                                "<td>"+this.details+"</td>"+
                                "<td>"+this.price+"元</td>"+
                                "<td>"+statement+"</td>"+
                                "<td align='center'><span onclick='del("+this.id+", this)'><div class='hlong'><img src='images/delete.png' ></span>"+
                                "</tr>";
                        }
                    })
                    $("tbody").append(str);

                }
            })
        })

        function del(id,obj) {

            if (confirm("确认删除吗？")){

                $.getJSON("orders/delete",{"id":id},function (data) {
                    if (data == 1) {
                        $(obj).parents("tr").remove();
                        alert("删除成功！")
                    }else {
                        alert("删除失败！")
                    }
                })

            }
        }

        function zhifu(id) {
            window.location.href = "to_alipay.do?out_trade_no="+id;
        }



        function changeA() {
            window.location.href = "personal.jsp";
            document.getElementById("menuA").style.background = '#FFD800';

            document.getElementById("menuB").style.background = '#FFC900';

            document.getElementById("menuC").style.background = '#FFC900';

        }

        function changeB() {
            window.location.href = "orders.jsp";
            document.getElementById("menuB").style.background = '#FFD800';

            document.getElementById("menuA").style.background = '#FFC900';

            document.getElementById("menuC").style.background = '#FFC900';

        }

        function changeC() {
            window.location.href = "myfavorite.jsp";
            document.getElementById("menuC").style.background = '#FFD800';

            document.getElementById("menuA").style.background = '#FFC900';

            document.getElementById("menuB").style.background = '#FFC900';

        }




</script>












<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>


</body>
</html>
