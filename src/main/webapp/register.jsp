<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/29
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>注册</title>
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" href="css/register.css">
    <!--导入jquery-->
    <script src="js/jquery-3.3.1.js"></script>
    <script type="text/javascript">

        var countdown=60;
        function settime(val) {
                if (countdown == 0) {
                val.removeAttribute("disabled");
                val.value="免费获取验证码";
                countdown = 60;
            } else {
                val.setAttribute("disabled", true);
                val.value="重新发送(" + countdown + ")";
                countdown--;
                setTimeout(function() {
                    settime(val)
                },1000)
            }
        }
        $(function () {
            $("input[id=sendsms]").click(function () {
                if(($("input[id=telephone]").val())==""){
                    alert("電話號碼不能為空！");
                }else {
                    var phone = $("input[id=telephone]").val();
                    $.getJSON("sms/send", {"phone": phone}, function (data1) {


                        $("input[id=tijiao]").click(function () {

                            var yzm = $("input[name=sms]").val();
                            if (yzm != data1) {
                                alert("验证码输入错误")
                            } else {
                                $.getJSON("user/phone", {"phone": phone}, function (data) {
                                    if (data == 1) {
                                        alert("该手机号已经被注册，请更换手机号！");
                                    } else {
                                        var username = $("input[name=username]").val();
                                        var password = $("input[name=password]").val();
                                        var email = $("input[name=email]").val();
                                        var name = $("input[name=name]").val();
                                        var telephone = $("input[name=telephone]").val();
                                        var sex = $("input[name=sex]").val();
                                        var birthday = $("input[name=birthday]").val();
                                        $.getJSON("user/register", {
                                            "username": username,
                                            "password": password,
                                            "email": email,
                                            "name": name,
                                            "telephone": telephone,
                                            "sex": sex,
                                            "birthday": birthday
                                        }, function (data2) {
                                            if (data2) {
                                                alert("注册成功！");
                                                window.location.href = "login.jsp";
                                            } else
                                                alert("注册失败！")
                                        })
                                    }
                                })
                            }
                        })
                    })
                }
            })

            $("input[name=username]").blur(function () {
                var username = $("input[name=username]").val();
                var uname = /^[a-zA-Z0-9]{6,10}$/;
                if (username == null || username == "") {
                    $("#error").html("用户名不能为空");
                }
                if (!uname.test(username)) {
                    $("#error").html("用户名必须是6-10位的英文和数字！")
                } else {
                    $("#error").html("")
                }
            })

            $("input[name=password]").blur(function () {
                var password = $("input[name=password]").val();
                var pwd = /^[a-z0-9_-]{6,18}$/
                if (password == null || password == "") {
                    $("#error").html("密码不能为空");
                }
                if (!pwd.test(password)) {
                    $("#error").html("密码由6-18位的字母和数字组成！")
                } else {
                    $("#error").html("")
                }
            })

            $("input[name=email]").blur(function () {
                var email = $("input[name=email]").val();
                var mail = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/
                if (email == null || email == "") {
                    $("#error").html("电子邮件不能为空");
                }
                if (!mail.test(email)) {
                    $("#error").html("请输入正确的电邮地址！")
                } else {
                    $("#error").html("")
                }
            })
            $("input[name=name]").blur(function () {
                var name = $("input[name=name]").val();
                var rname = /^[\u4E00-\u9FA5A-Za-z]+$/
                if (name == null || name == "") {
                    $("#error").html("姓名不能为空");
                }
                if (!rname.test(name)) {
                    $("#error").html("请输入正确的姓名！")
                } else {
                    $("#error").html("")
                }
            })
            $("input[name=telephone]").blur(function () {
                var telephone = $("input[name=telephone]").val();
                var phone = /^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/
                if (telephone == null || telephone == "") {
                    $("#error").html("手机号不能为空");
                }
                if (!phone.test(telephone)) {
                    $("#error").html("请输入正确的手机号！")
                } else {
                    $("#error").html("")
                }
            })
        })
    </script>
</head>
<body>
<!--引入头部-->
<div id="header"></div>
<!-- 头部 end -->
<div class="rg_layout">
    <div class="rg_form clearfix">
        <div class="rg_form_left">
            <p>新用户注册</p>
            <p>USER REGISTER</p>
            <div>
                <span id="error" style="color: red"></span>
            </div>
        </div>
        <div class="rg_form_center">
            <!--注册表单-->
            <form id="registerForm">
                <!--提交处理请求的标识符-->
                <input type="hidden" name="action" value="register">
                <table style="margin-top: 25px;">
                    <tr>
                        <td class="td_left">
                            <label for="username">用户名</label>
                        </td>
                        <td class="td_right">
                            <input type="text" id="username" name="username" placeholder="请输入账号">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left">
                            <label for="password">密码</label>
                        </td>
                        <td class="td_right">
                            <input type="password" id="password" name="password" placeholder="请输入密码">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left">
                            <label for="email">Email</label>
                        </td>
                        <td class="td_right">
                            <input type="text" id="email" name="email" placeholder="请输入Email">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left">
                            <label for="name">姓名</label>
                        </td>
                        <td class="td_right">
                            <input type="text" id="name" name="name" placeholder="请输入真实姓名">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left">
                            <label for="telephone">手机号</label>
                        </td>
                        <td class="td_right">
                            <input type="text" id="telephone" name="telephone" placeholder="请输入您的手机号">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left">
                            <label for="sms">验证码</label>
                        </td>
                        <td class="td_right">
                            <input type="text" id="sms" name="sms"  placeholder="请输入验证码">
                        </td>
                        <td class="td_right">
                            <input type="button" id="sendsms" value="获取验证码"
                                   style="width: 80px; height: 32px; text-align: center; font-size: 12px; border-radius: 2px" onclick="settime(this)">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left">
                            <label for="sex">性别</label>
                        </td>
                        <td class="td_right gender">
                            <input type="radio" id="sex" name="sex" value="男" checked> 男
                            <input type="radio" name="sex" value="女"> 女
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left">
                            <label for="birthday">出生日期</label>
                        </td>
                        <td class="td_right">
                            <input type="date" id="birthday" name="birthday" placeholder="年/月/日" value-max="2019-11-15" aria-valuemin="1919-11-15">
                        </td>
                    </tr>

                    <tr>
                        <td class="td_left">
                        </td>
                        <td class="td_right check">
                            <input type="button" class="submit" id="tijiao" value="注册">
                            <span id="msg"></span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="rg_form_right">
            <p>
                已有账号？
                <a href="login.jsp">立即登录</a>
            </p>
        </div>
    </div>
</div>
<!--引入尾部-->
<div id="footer"></div>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
</body>
</html>