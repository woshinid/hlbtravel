
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/29
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>胡萝卜旅游网-我的收藏</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" href="css/index.css">
    <style>
        .X1{
            width: 299px;
            height: 169px;
        }

        .tab-content .row > div {
            margin-top: 16px;

        }

        .tab-content {
            margin-bottom: 36px;
        }
        li{
            float: left;
        }
    </style>
    <script src="js/jquery-3.3.1.js"></script>

    <script>


        var pn = 1;
        var ps = 10;
        $(function () {
            query(pn, ps);
        });


        function query(pn, ps) {
            $.getJSON("travel/favoritelist", {"pn": pn, "ps": ps, "uid":${sessionScope.user.id}}, function (data) {
                var obj = eval(data);
                var str = "";
                $(obj.list).each(function () {
                    str +="<div name='delFavorite'><div class='col-md-3'>"
                        +"<a href='route_detail.jsp?id=" + this.id + "'>"
                        + "<img src=" + this.picture + " alt='' class='X1'></a>"
                        + "<div class='has_border'>"
                        + "<h3>" + this.discount + "</h3>"
                        + "<div class='price'>网付价"
                        + "<em>￥</em><strong>" + this.price + "</strong><em>"
                        + "起</em></div>"
                        + "</div><div><input type='button' onclick='del("+this.id+",this)' value='是否移除收藏？'></div></a></div></div>"
                })

                $(".row").empty();
                $(".row").append(str);

                var page = "";
                page += "<ul class=\"pagination\">" +
                    "<li><a href=\"javascript:void(0)\" onclick='query(1," + ps + ")'>首页</a></li>";
                if (obj.hasPreviousPage) {
                    page += "<li>" +
                        "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"query(" + (obj.pageNum - 1) + "," + ps + ")\">" +
                        "<span aria-hidden=\"true\">&laquo;</span>" +
                        "</a>" +
                        "</li>";
                }

                $(obj.navigatepageNums).each(function () {
                    if (obj.pageNum == this) {
                        page += "<li class='active'><a href=\"javascript:void(0)\" onclick='query(" + this + "," + ps + ")'>" + this + "</a></li>";
                    } else {
                        page += "<li><a href=\"javascript:void(0)\" onclick='query(" + this + "," + ps + ")'>" + this + "</a></li>";
                    }

                })

                if (obj.hasNextPage) {
                    page += "<li>\n" +
                        "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='query(" + (obj.pageNum + 1) + "," + ps + ")'>" +
                        "<span aria-hidden=\"true\">&raquo;</span>" +
                        "</a>" +
                        "</li>";
                }
                page += "<li><a href=\"javascript:void(0)\" onclick='query(" + obj.pages + "," + ps + ")'>未页</a></li>" +
                    "</ul>";
                $("nav").empty();
                $("nav").append(page);

            });
        }
        function del(id,obj) {
            $.getJSON("travel/delete",{"id":id,"uid":${sessionScope.user.id}},function (data) {
                if (data){
                   if (confirm("是否确定删除？")){
                       $(obj).parents("div[name=delFavorite]").remove();
                   }
                }
            })
        }

    </script>
</head>
<body>
<!--引入头部-->
<div id="header"></div>
<!-- 排行榜 start-->
<section id="content">
    <section class="hemai_jx">
        <div class="jx_top">
            <div class="jx_tit">
                <img src="images/icon_5.jpg" alt="">
                <span>我的收藏</span>
            </div>
        </div>


        <div class="jx_content">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div class="row">

                    </div>
                </div>
            </div>
        </div>

    </section>
</section>
<!-- 排行榜 end-->

<!--引入尾部-->
<div id="footer"></div>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
</body>
</html>