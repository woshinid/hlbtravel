<%--
  Created by IntelliJ IDEA.
  User: 19245
  Date: 2019/11/9
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="css/style.css" />
</head>
<body style="font-family: Arial, Helvetica, sans-serif;background-image:url('images/3.jpg'); background-size: cover; height: 100%;">
<div class="login-section w3l">
    <div class="sig-top agileits">
        <div class="clear"></div>

        <div class="clear"></div>
    </div>
    <div class="sig-btm w3agile">
        <div class="signup">
            <div class="modal-content modal-info">
                <div class="modal-header">
                    <h3>CHENGE PASSWORD ?</h3>
                </div>
                <div class="modal-body modal-spa">
                    <div class="login-form">
                        <form action="#" method="post" id="signup">
                            <ol>
                                <li>
                                    <input type="password" class="oldPassWord" id="telephone" name="email" placeholder="请输入新的密码" maxlength="11">
                                    <input type="password" class="newPassWord" id="sms" name="email" placeholder="请确认输入的密码">
                                </li>
                            </ol>
                            <input type="button" iconCls="icon-vfp-save" id="sure-button" class="class-btn class-btn-sure" value="确认"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <p class="footer" style="position: absolute;bottom: 10px;">江苏胡萝卜文化旅游股份有限公司 版权所有&copy;Copyright 2006-2019, All Rights Reserved 苏ICP备16007882</p>
</div>
<script src="js/jquery-3.3.1.js"></script>
<script>
    $(function () {

        $("input[id=sure-button]").click(function () {
            var pwd1 = $("input[class=oldPassWord]").val();
            var pwd2 = $("input[class=newPassWord]").val();
            if (pwd1 == "" || pwd2 == ""){
                alert("密码不能为空！");
            } else if (pwd2 != pwd1){
                alert("两次密码不一致，请重新输入！")
            } else {
                $.getJSON("user/updatepassword",{"password":pwd1,"phone":${param.phone}},function (data) {
                    if (data==1){
                        alert("修改成功");
                        window.location.href = "login.jsp";
                    }
                })
            }
        })
    })
</script>
</body>
</html>
