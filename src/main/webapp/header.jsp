<%@ taglib prefix="v-on" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script src="js/jquery-3.3.1.js" type="text/javascript"></script>
<script src="js/vue.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/common.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="js/include.js">
<link rel="stylesheet" href="src/jquery.typeahead.css">
<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="src/jquery.typeahead.js"></script>

<script type="text/javascript">

    $(function () {
        $("button[id=search-icon]").click(function () {

            var details = $("input[class=search_input]").val();

            window.location.href = "search_list.jsp?details="+details;

        })

        if (${empty sessionScope.user}){
            $("div[class=login_out]").show();
            $("div[class=login]").hide();
        }

        if (${not empty sessionScope.user}){
            $("div[class=login_out]").hide();
            $("div[class=login]").show();
        }


    });

    function out() {
        var result = confirm("确定要退出吗？");
        if (result) {
            window.location.href = "user/tologout";
        }

    }
</script>

<!-- 头部 start -->
<header id="header">
    <div class="top_banner">
        <span class="login_left">去见证更美的世界</span>
        <div class="shortcut">
            <!-- 未登录状态  -->

            <div class="login_out">
                <a href="login.jsp">登录</a>
                <a href="register.jsp">注册</a>
                <a href="adminLogin.jsp">管理员登录</a>
            </div>
            <!-- 登录状态  -->
            <div class="login">
                <span>欢迎 : ${sessionScope.user.username}</span>

                <a href="personal.jsp" class="collection">个人中心</a>
                <%-- <a href="orders.jsp" style="color: black;" >我的订单</a>--%>
                <a href="index.jsp" onclick="out()">退出</a>
            </div>
        </div>
    </div>
    <div class="header_wrap">
        <div class="topbar">
            <div class="logo">
                <a href="index.jsp"><img src="images/logo1.jpg" alt=""></a>
            </div>

            <div class="sousuo">
                <form style="position:absolute;z-index:100">
                    <div class="typeahead__container">
                        <div class="typeahead__field">
							<span class="typeahead__query">
								<input placeholder="请输入路线名称" class="search_input" name="q" type="search" autofocus
                                       autocomplete="off">
							</span>
                            <span class="typeahead__button">
								<button type="button" id="search-icon">
									<span class="typeahead__search-icon"></span>
								</button>
							</span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="hottel">
                <div class="hot_tel">
                    <p class="hot_time">客服热线(9:00-6:00)</p>
                    <p class="hot_num">400-618-9090</p>
                </div>
                <div class="hot_pic">
                    <img src="images/hot_tel.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</header>
<!-- 头部 end -->
<!-- 首页导航 -->
<div class="navitem">
    <ul class="nav">
        <li class="nav-active" ><a href="index.jsp">首页</a></li>
        <li class="nav-active"><a href="route_list2.jsp">出境游</a></li>
        <li class="nav-active"><a href="route_list1.jsp">国内游</a></li>
        <li class="nav-active"><a href="route_list.jsp">全球自由行</a></li>
    </ul>
</div>

