<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/11/4
  Time: 6:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/add2.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--导入angularJS文件-->
    <%--<script src="js/angular.min.js"></script>--%>
    <!--导入jquery-->

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
        $(function(){
            var id = getUrlParam("id");

            $.getJSON("details/queryById", {"id": id}, function(data){
                $("input[name=id]").val(data.id);
                $("input[name=brief]").val(data.brief);
                $("input[name=details]").val(data.details);
                $("input[name=price]").val(data.price);
                $("textarea[class=a530_]").val(data.discount);
                $("input[name=picture]").val(data.picture);

            });

            $("input[type=button]:first").click(function(){
               /* var discount = $("textarea[class=a530_]").val();*/
                $.getJSON("details/update", $("form").serialize()+"&id="+id, function(data){
                    if(data){
                        alert("修改成功！");
                        window.location.href = "details2.jsp";
                    }else{
                        alert("修改失败！");
                    }
                });
            })

        })

        //获取地址栏参数,可以是中文参数
        function getUrlParam(key) {
            // 获取参数
            var url = window.location.search;
            // 正则筛选地址栏
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
            // 匹配目标参数
            var result = url.substr(1).match(reg);
            //返回参数值
            return result ? decodeURIComponent(result[2]) : null;
        }
        function testA() {
            $("#NO0").toggle();
        }
        function testB() {
            $("#NO1").toggle();
        }
        function testC() {
            $("#NO2").toggle();
        }
    </script>
</head>


<body>
<div id="zong_112">
    <div class="av9_"><span>胡萝卜网首页</span></div>
    <div class="right09_"><a href="">登录</a>&nbsp;&nbsp;<a href="">注册</a>&nbsp;&nbsp;<a href="">管理员登陆</a></div>
</div>
<div id="top3o_">
    <div class="tleft">
        <a class="tf" href=""><img src="image/5.jpg" alt=""></a>
    </div>
    <div class="tmiddle">
        <input class="tm" type="text" placeholder="请输入路线名称">
        <div class="tmr">
            <a  class="ai_" href="">搜索</a>
        </div>
    </div>
    <div class="tright">
        <img class="img_" src="image/hot_tel.jpg" alt="">
        <p class="topword">客服热线:9:00-6:00</p>
        <p class="downword">400-618-9090</p>
    </div>
</div>

<div id="titles0_">
    <div class="ad124">
        首页
    </div>
    <div class="ad134">国内游</div>
    <div class="ad144">国外游</div>
    <div class="ad154">我的收藏</div>
</div>
<div class="kongbai"></div>
<div class="toubu_">
    <div class="left_2a"><span class="wel">欢迎您,${manager.mname}</span></div>



</div>


<div class="mainlr">
    <div class="leftia_">
        <div id="menu">
            <h1 onclick="testA()"> 商品信息管理</h1>
            <span id="NO0" class="no">




	<h2><a href="add2.jsp">商品信息增加</a></h2>

	  <h2><a href="delete.jsp">商品修改删除</a></h2>
	  <h2><a href="details2.jsp">商品信息列表</a></h2>
	</span>
            <h1 onclick="testB()"> 订单信息管理 </h1>
            <span id="NO1" class="no">



	<h2><a href="dingDanList.jsp">订单信息列表</a></h2>
	  <h2><a href="delete2.jsp">订单修改删除</a></h2>


	</span>
            <h1 onclick="testC()" > 用户信息管理</h1>
            <span id="NO2" class="no">
<!-- 	 <a href="">用户信息列表</a>
	 <a href="">用户信息删除</a> -->
	<h2>  <a href="userList2.jsp">用户信息列表</a></h2>
	  <h2> <a href="delete3.jsp">用户信息修改</a></h2>

	</span>

        </div>

    </div>
    <div class="righis_">
        <span class="asd1d_">修改商品信息</span>
        <form action="">
           <%-- 编号:<input class="a23a" type="text" name="id" value="" /><br>--%>
            简介:<input class="a23a" type="text" name="brief" value="" /><br>
            标题:<input class="a23a" type="text" name="details" value="" /><br>
            价格:<input class="a23a" type="text" name="price" value="" /><br>
            详情:<textarea class="a530_"  rows="3" cols="2" name="discount"></textarea><br>
            图片:<input class="awia"  type="file" name="picture"  value="" /><br>
            <input class="f90a_" type="button" name=""  value="提交" />
            <input class="f90a_" type="button" name=""  value="返回" onclick="window.history.back()"/>
        </form>

    </div>

</div>
</div>
<!-- 底部 -->
<div style="height:80px"></div>
<div class="adw8">
    <ul>
        <li><a href="">关于我们</a></li>
        <li><a href="">About us</a></li>
        <li><a href="">Investor Relations</a></li>
        <li><a href="">媒体报道</a></li>
        <li><a href="">品牌招商</a></li>
        <li><a href="">隐私条款</a></li>
        <li><a href="">胡萝卜诚聘</a></li>
        <li><a href="">联系我们</a></li>
    </ul>
</div>
<div class="footmain">
    <img src="image/2ac.png" alt="">

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
</body>
</html>
