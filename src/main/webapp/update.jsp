<%--
  Created by IntelliJ IDEA.
  User: 19245
  Date: 2019/11/4
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="css/personal.css" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/Order.css" />
    <link rel="stylesheet" type="text/css" href="css/update.css">
    <style>
        .hlong *{display:inline-block;vertical-align:middle}
    </style>
</head>

<body>
<!--引入头部-->
<div id="header"></div>

<!--个人资料-->
<div style="width:100% ; height: 550px;background: url(images/bgbg1.png);background-size: 100% 100%;">
    <ul class="sort">
        <li>
            <div style="height: 18px;"></div>
            <div class="navigation" style="box-shadow: 0px 0px 20px darkgrey;">
                <br />
                <div class="perinfo">
                    <div id="uploadify-button" class="uploadify-button " style="height: 80px; line-height: 80px; width: 80px;">
                        <img  src="images/huluobo.png" style="width: 80px;height: 80px;margin-bottom:  5px;border-radius: 40px;margin-left: 33px;">
                    </div>
                    <a><img  src="images/shou1.png" style="width: 30px;height:30px;margin-bottom: 15px;border-radius: 40px;margin:auto;margin-top: 25px;"/></a>
                    <button id="menuA" href="#" onclick="changeA()"><div class="hlong"><img src="images/small1.png"><span style="color: #5e5e5e">个人信息</span></div></button>
                    <button id="menuB" href="#" onclick="changeB()"><div class="hlong"><img src="images/small2.png"><span style="color: #5e5e5e">我的订单</span></div></button>
                    <button id="menuC" href="#" onclick="changeC()"><div class="hlong"><img src="images/small3.png"><span style="color: #5e5e5e">我的收藏</span></div></button>

                </div>
            </div>
        </li>
        <li>
            <div style="height: 18px;"></div>
            <div class="data_box" style="box-shadow: 0px 0px 20px darkgrey;background-color: white;">
                <div style="height: 25px;"></div>
                <div class="show_data" style="text-align: center; ">
                    <div class="tbody" style="text-align: center;font-size: 20px;line-height: 30px;margin-left:200px ;">

                        <form>
                            <%-- <input type="hidden" name="id" value="${param.id }"/> --%>
                            <table class="abcdefg">
                                <tr  style="text-align: center;"><td colspan="2" style="font-weight: bold;color: #FFC900;font-size: 25px;padding-left: 70px">修改资料</td></tr>
                                <tr>
                                    <td style="text-align: right;">用户名：</td>
                                    <td><input type="text" name="username" /> </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Email：</td>
                                    <td><input type="text" name="email"/> </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">真实姓名：</td>
                                    <td><input type="text" name="name"/> </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">生日：</td>
                                    <td><input type="date" name="birthday"/> </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">性别：</td>
                                    <td><input type="text" name="sex"/> </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">电话号码：</td>
                                    <td><input type="text" name="telephone"/> </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2" style="padding-left: 48px">
                                        <input type="button" name="tijiao" value="提交" style="font-size: 16px;line-height: 20px;width: 75px;" />

                                        <input type="button" value="返回" onclick="window.history.back()" style="font-size: 16px;line-height: 20px;width: 75px;"/>
                                    </td>
                                </tr>
                            </table>

                        </form>

                    </div>
<%--                    <a href="#" style="color: #0000ff;">[修改个人信息]</a>--%>

                </div>
            </div>
            <div style=" height: 18px;"></div>
        </li>
    </ul>
</div>
<!--导入底部-->
<div id="footer"></div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
<script>
    $(function(){
        var id = getUrlParam("id");

        $.getJSON("user/queryById", {"id": id}, function(data){
            $("input[name=username]").val(data.username);
            $("input[name=email]").val(data.email);
            $("input[name=name]").val(data.name);
            $("input[name=birthday]").val(data.birthday);
            $("input[name=sex]").val(data.sex);
            $("input[name=telephone]").val(data.telephone);
        });

        $("input[name=tijiao]").click(function(){

            $.getJSON("user/update", $("form").serialize()+"&id="+id, function(data){
                if(data){
                    alert("修改成功！");
                    window.location.href = "personal.jsp";
                }else{
                    alert("修改失败！");
                }
            });
        })

    })

    //获取地址栏参数,可以是中文参数
    function getUrlParam(key) {
        // 获取参数
        var url = window.location.search;
        // 正则筛选地址栏
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
        // 匹配目标参数
        var result = url.substr(1).match(reg);
        //返回参数值
        return result ? decodeURIComponent(result[2]) : null;
    }
</script>
<script type="text/javascript">
    function changeA() {
        window.location.href = "personal.jsp";
        document.getElementById("menuA").style.background = '#FFD800';

        document.getElementById("menuB").style.background = '#FFC900';

        document.getElementById("menuC").style.background = '#FFC900';

    }

    function changeB() {
        window.location.href = "orders.jsp";
        document.getElementById("menuB").style.background = '#FFD800';

        document.getElementById("menuA").style.background = '#FFC900';

        document.getElementById("menuC").style.background = '#FFC900';

    }

    function changeC() {
        window.location.href = "myfavorite.jsp";
        document.getElementById("menuC").style.background = '#FFD800';

        document.getElementById("menuA").style.background = '#FFC900';

        document.getElementById("menuB").style.background = '#FFC900';

    }
</script>
</body>
</html>
