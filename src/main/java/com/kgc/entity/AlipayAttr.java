package com.kgc.entity;

public class AlipayAttr {
	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016101600703135";

	// 商户应用私钥，您的PKCS8格式RSA2私钥
	public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCLFFRgACz37ae/EiH5mHIARdNtPs8A51FPYTrKSGQ9ft6zUBWtL1SWpOy3ShiSfgnZOu5XuLUxr4gDc4t6ieuVDHdY1lMWSRDCD+5tvWKXkeRinsIpXq3EvUP7INnI8cxxWBoUWE+QYDoBAy4aFbOLEqAg0LKbORutoySE1aKBJpWV6xYZ864kWZeQWpMPQYzeyI1b+x53RXmWUrQMvW1bKeuJajfunG5JOwwxoVrtxsNioGFE0L5w07Z1u00GQ086CvByBpiDI9br1XWzo5ay0SxQqi5spDCe6TwWudRyN6zCNbklhArJOXowG/nHY1bd3pUL+fTFQnIggFDRKjuPAgMBAAECggEATWP+n3z5iOyhz1zTswP1S9mPENdGcQZG5JkCEdW+kcmrOrRxm3Ee7MTRRBzjF4EzEcINJpsNmJxIX0NIdeORR9sr/i8lBN2biaE716zu09DG+WnGOScKItP+pBtflMWYMeoIeRG7FGe3XyDZ4oH46oZuvFPlTyK0GCcUByapyptOMyFaA4lr6YoBrEdOUH8R2JiQKr7FthWje/I0+14Ck9EFlv8hmvy6YwMUv0rHvlN7GF6yhHhxhQpmJ+OgAji/dZ5e99NR17yxAbw6OuWE3dvxQND+ALGSb8CTfYZqQbvlZFuphKjCehxtlGXtrFqes67UsaX6Av0hZTUwR36FAQKBgQDV5nZY6JkkMcESfoBN470fccIkeCjrPrNypXYQB3M+rOB/rixygPkKX4lTCWtQjMoS/p6E0JdLnFRaq9HrBmBRCrvjFvRUCF1xAZGHBcjsh4y9RKpKP3jmFVSAz3VJ0xdlR8smKReXZgoL3spmBYrhxy5zJq+hf/ho0Z9PDAAxbwKBgQCmc/V5oKLnQ0hUneC103NWtkRuiesTQrYusLBEytXkUE7NfQehP0NDyrZbqf9ZPwQ0TwwlYc/FkdZpDeEg0HSLdrrffJUFiDw9/0nF6flUQBi4ab2wN7reB375ryXvvAhjx8u3O/q8NI2VV5yloLWzoiXPgd7W9NHTYRWW1sdH4QKBgQCMpoxvjhMVH7pLRO9tSDFV+eGlRXXyqjopSu/FPoDjV54PlyIW8QPRdFzCQ1FTmCBQOSlXQCocR+GX9Z+FIZ437Az8GF3028NFybg9xuoFzmQXGI25YN9IvAHi6YfAUZPyvzCwGRkwLqKwh4EjcMWQDP9YLLoUQifAFQbiXYvK9QKBgQCGSJBlwgEJ/w6wLPnm1iJ7dum4H43E6RHeku5xYQ2zdlJMlqx8GnRikXwScQ+uaRc0N/09fy5GXkQSVn8joOgUxwE1YOU8JE8tWPGkOADMRjVrOOst6ZfYnUWDmxhI0nIWRU4Ekd/Sy7bZH2gheoh71bkZMdWbpsBL/kuTd8wnIQKBgCJXDKGBO8FNdQjCYh1r06hPsCTjxET6jtFyemjulLk9Gdwq45XJQ2Dd3M1aV2ePocEGlbaV7183HdoDLwT4WxCi6ySBAkrEH5sJ9jSyJ0vWg2kYRmXZYCsV34RYnznfnGRdH6twG37xAnGePkLd5EIIBvYPXwuiwRbnBEwK1Tbv";

	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm
	// 对应APPID下的支付宝公钥。
	public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuIBQHK2mrE4XBFxW3LwwYZKvHy1ZFzYWAruDsTcbhiMC4KUefPmEira384vQZfauYtLKp3h5gQiyENXLvAb7ADZbKEC4hV4mkcIFAL4qhpCaeW6fB2MZZMc8mu5EALuq26ALDnpATrPzmexIcCIDtaoJD4GwQ7Lw3xYZy61Hy+PMhPM3ctSitay8qrf9tU0ZejuhbMghax6qoe4cO5t13poleeXkFcdiyXCX76ub8Jg/iAmQ/FHOG6Ay004c2HRkHwNISpiHW+Ghr0UZnqgXTjPu2aAoxwhjGNv79/GTujVYXSVNrVJSOq1zfH30xdM5+/aQBNNSw4T4nIyUU3QELQIDAQAB+PMhPM3ctSitay8qrf9tU0ZejuhbMghax6qoe4cO5t13poleeXkFcdiyXCX76ub8Jg/iAmQ/FHOG6Ay004c2HRkHwNISpiHW+Ghr0UZnqgXTjPu2aAoxwhjGNv79/GTujVYXSVNrVJSOq1zfH30xdM5+/aQBNNSw4T4nIyUU3QELQIDAQAB+PMhPM3ctSitay8qrf9tU0ZejuhbMghax6qoe4cO5t13poleeXkFcdiyXCX76ub8Jg/iAmQ/FHOG6Ay004c2HRkHwNISpiHW+Ghr0UZnqgXTjPu2aAoxwhjGNv79/GTujVYXSVNrVJSOq1zfH30xdM5+/aQBNNSw4T4nIyUU3QELQIDAQAB";

	// 服务器异步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8080/Alipay/notify_url.do";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8080/maven_demo_war_exploded/alipay_return.do";

	// 签名方式
	public static String sign_type = "RSA2";

	// 字符编码格式
	public static String charset = "utf-8";

	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
}
