package com.kgc.service;

import com.kgc.entity.User;

import java.util.List;

public interface UserService1 {
     User queryByNameAndPassword(String username, String password);
     public List<User> queryAll();
     public boolean update(User user);
     public User queryById(int id);
     int getDetailsById(int id);

}
