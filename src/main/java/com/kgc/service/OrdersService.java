package com.kgc.service;

import com.kgc.entity.Orders;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrdersService {

    int addOrders(Orders orders);


    List<Orders> getOrdersList(int uid);

    int deleteOrders(String id);

    int updateState( String id);

    Orders getOrdersById(String id);


}
