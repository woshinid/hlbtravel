package com.kgc.service.impl;

import com.kgc.entity.Manager;
import com.kgc.mapper.ManagerMapper;
import com.kgc.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerServiceImpl implements ManagerService {
    @Autowired
    private ManagerMapper managerMapper;
    @Override
    public Manager query(String mname, String mpassword) {
        return managerMapper.query(mname,mpassword);
    }
}
