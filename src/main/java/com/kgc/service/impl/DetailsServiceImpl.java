package com.kgc.service.impl;

import com.kgc.entity.Details;
import com.kgc.mapper.DetailsMapper;
import com.kgc.service.DetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DetailsServiceImpl implements DetailsService {
    @Autowired
    private DetailsMapper detailsMapper;

    @Override
    public List<Details> queryAll() {
        return detailsMapper.queryAll();
    }

    @Override
    public boolean add(Details details) {
        return detailsMapper.add(details) == 1 ? true : false;
    }

    @Override
    public boolean delete(int id) {
        return detailsMapper.delete(id) == 1 ? true : false;
    }

    @Override
    public boolean update(Details details) {
        return detailsMapper.update(details) == 1 ? true : false;
    }

    @Override
    public Details queryById(int id) {
        return detailsMapper.queryById(id);
    }

    @Override
    public boolean update1(int id) {
        return detailsMapper.update1(id); }

    @Override
    public int getDetailsById(int id) {
        return detailsMapper.getDetailsById(id);
    }


}
