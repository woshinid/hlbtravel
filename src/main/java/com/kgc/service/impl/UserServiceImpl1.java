package com.kgc.service.impl;

import com.kgc.entity.User;
import com.kgc.mapper.UserMapper1;
import com.kgc.service.UserService1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl1 implements UserService1 {
    @Autowired
    private UserMapper1 userMapper;

    @Override
    public User queryByNameAndPassword(String username, String password) {

        return userMapper.queryByNameAndPassword(username, password);
    }

    @Override
    public List<User> queryAll() {
        return userMapper.queryAll();
    }

    @Override
    public boolean update(User user) {
        return userMapper.update(user) == 1 ? true : false;
    }

    @Override
    public User queryById(int id) {
        return userMapper.queryById(id);
    }

    @Override
    public int getDetailsById(int id) {
        return userMapper.getDetailsById(id);
    }
}
