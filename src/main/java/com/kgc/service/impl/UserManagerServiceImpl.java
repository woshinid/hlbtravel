package com.kgc.service.impl;

import com.kgc.entity.UserManager;
import com.kgc.mapper.UserManagerMapper;
import com.kgc.service.UserManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserManagerServiceImpl implements UserManagerService {
    @Autowired
    private UserManagerMapper userManagerMapper;
    @Override
    public List<UserManager> queryAll() {
        return userManagerMapper.queryAll();
    }

    @Override
    public boolean update(UserManager userManager) {
        return userManagerMapper.update(userManager)==1?true:false;
    }

    @Override
    public boolean delete(int id) {
        return userManagerMapper.delete(id)==1?true:false;
    }

    @Override
    public UserManager queryById(String id) {
        return userManagerMapper.queryById(id);
    }

    @Override
    public int getDetailsById(int id) {
        return userManagerMapper.getDetailsById(id);
    }
}
