package com.kgc.service;

import com.kgc.entity.Details;

import java.util.List;

public interface DetailsService {
    public List<Details> queryAll();
    public boolean add(Details details);
    public boolean delete(int id);
    public boolean update(Details details);
    public Details queryById(int id);
    public boolean update1(int id);
    int getDetailsById(int id);

}
