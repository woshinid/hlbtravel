package com.kgc.service;

import com.kgc.entity.UserManager;

import java.util.List;

public interface UserManagerService {
    public List<UserManager> queryAll();
    public boolean update(UserManager userManager);
    public boolean delete(int id);
    public UserManager queryById(String id);
    int getDetailsById(int id);

}
