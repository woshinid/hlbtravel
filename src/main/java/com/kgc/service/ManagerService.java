package com.kgc.service;

import com.kgc.entity.Manager;

public interface ManagerService {
    public Manager query(String mname, String mpassword);
}
