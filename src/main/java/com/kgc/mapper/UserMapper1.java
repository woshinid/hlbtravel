package com.kgc.mapper;

import com.kgc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper1 {

    User queryByNameAndPassword(@Param("name") String name, @Param("pwd") String pwd);
    public List<User> queryAll();
    public int update(@Param("user") User user);
    public User queryById(@Param("id") int id);
    int getDetailsById(@Param("id") int id);

}
