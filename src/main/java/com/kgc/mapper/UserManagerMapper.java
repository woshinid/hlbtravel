package com.kgc.mapper;

import com.kgc.entity.UserManager;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserManagerMapper {
    public int delete(@Param("id") int id);
    public List<UserManager> queryAll();
    public int update(@Param("userManager") UserManager userManager);

    public UserManager queryById(@Param("id") String id);
    int getDetailsById(@Param("id") int id);
}
