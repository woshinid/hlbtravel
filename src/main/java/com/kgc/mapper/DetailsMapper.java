package com.kgc.mapper;

import com.kgc.entity.Details;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DetailsMapper {
    public List<Details> queryAll();
    public int add(@Param("details") Details details);
    public int delete(@Param("id") int id);
    public int update(@Param("details") Details details);
    public Details queryById(@Param("id") int id);
    public boolean update1(@Param("id") int id);
    int getDetailsById(@Param("id") int id);


}
