package com.kgc.mapper;

import com.kgc.entity.Manager;
import org.apache.ibatis.annotations.Param;

public interface ManagerMapper {
    public Manager query(@Param("name") String name, @Param("pwd") String mpassword);
}
