package com.kgc.servlet;

import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.kgc.entity.AlipayAttr;
import com.kgc.entity.Orders;
import com.kgc.entity.User;
import com.kgc.mapper.OrdersMapper;
import com.kgc.service.OrdersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class AlipayServlet {


	@Autowired
	private OrdersService ordersService;


	@RequestMapping("to_alipay.do")
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// 处理乱码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		/*获得初始化的AlipayClient
		 * AlipayClient alipayClient = new DefaultAlipayClient(
		 * "https://openapi.alipaydev.com/gateway.do",
		 * "2016091700535499","请复制第1步中生成的密钥中的商户应用私钥,
		 * "json","utf-8","沙箱环境RSA2支付宝公钥","RSA2");
		 * */

		AlipayClient alipayClient = new DefaultAlipayClient(
				AlipayAttr.gatewayUrl, AlipayAttr.app_id,
				AlipayAttr.merchant_private_key, "json", AlipayAttr.charset,
				AlipayAttr.alipay_public_key, AlipayAttr.sign_type);
		String out_trade_no = null;

		if ( request.getParameter("out_trade_no")!=null){
			out_trade_no = request.getParameter("out_trade_no");
			Orders orders = ordersService.getOrdersById(out_trade_no);
			String in_name = orders.getUname();
			String in_phone = "18852734236";
			String subject = in_name+"的订单";
			String total_amount = orders.getPrice();
			String details = orders.getDetails();
			int person = orders.getPerson();
			String time = orders.getTime();
			int uid = orders.getUid();
			AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
			alipayRequest.setReturnUrl(AlipayAttr.return_url);//支付成功响应后跳转地址
			alipayRequest.setNotifyUrl(AlipayAttr.notify_url);//异步请求地址

			/*FAST_INSTANT_TRADE_PAY 二维码瞬时支付
			 * out_trade_no 订单号 total_amount 订单金额  subject 订单名称
			 */
			alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no
					+ "\"," + "\"total_amount\":\"" + total_amount + "\","
					+ "\"subject\":\"" + subject + "\"," + "\"body\":\""
					+ "" + "\"," + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
			String result = "请求无响应";

			try {




				//通过阿里客户端，发送支付页面请求
				result = alipayClient.pageExecute(alipayRequest).getBody();
				response.getWriter().println(result);
				response.getWriter().flush();
			} catch (AlipayApiException e) {
				e.printStackTrace();
			} finally {
				response.getWriter().close();
			}




		}else {


			// 取购买人名称
			String in_name = request.getParameter("in_name");
			// 取手机号
			String in_phone = request.getParameter("in_phone");
			// 创建唯一订单号
			int random = (int) (Math.random() * 10000);
			String dateStr = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

			// 订单号拼接规则：手机号后四位+当前时间后四位+随机数四位数
			out_trade_no = in_phone.substring(7) + dateStr.substring(10)
					+ random;


			// 拼接订单名称
			String subject = in_name + "的订单";
//		String subject = "大喵的订单";

			// 取付款金额
			String total_amount = request.getParameter("in_money");


			String details = request.getParameter("details");

			int person = Integer.parseInt(request.getParameter("person"));
			String time = request.getParameter("time");
			int uid = Integer.parseInt(request.getParameter("uid"));

			// 设置请求参数
			AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
			alipayRequest.setReturnUrl(AlipayAttr.return_url);//支付成功响应后跳转地址
			alipayRequest.setNotifyUrl(AlipayAttr.notify_url);//异步请求地址

			/*FAST_INSTANT_TRADE_PAY 二维码瞬时支付
			 * out_trade_no 订单号 total_amount 订单金额  subject 订单名称
			 */
			alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no
					+ "\"," + "\"total_amount\":\"" + total_amount + "\","
					+ "\"subject\":\"" + subject + "\"," + "\"body\":\""
					+ "" + "\"," + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
			String result = "请求无响应";
			// 请求
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				Orders orders = new Orders();
				orders.setId(out_trade_no);
				orders.setDetails(details);
				orders.setPublishdate(format.format(new Date()));
				orders.setPrice(total_amount);
				orders.setUname(in_name);
				orders.setPerson(person);
				orders.setTime(time);
				orders.setUid(uid);


				//ordersService.addOrders(orders);
				ordersService.addOrders(orders);


				//通过阿里客户端，发送支付页面请求
				result = alipayClient.pageExecute(alipayRequest).getBody();
				response.getWriter().println(result);
				response.getWriter().flush();
			} catch (AlipayApiException e) {
				e.printStackTrace();
			} finally {
				response.getWriter().close();
			}
		}
	}

	}


