package com.kgc.controller;

import com.kgc.entity.User;
import com.kgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.util.List;

@Controller
@RequestMapping("user")
@ResponseBody
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("register")
    public  boolean testRegister(User user){
        return userService.registerUser(user)==1?true:false;
    }


    @RequestMapping("login")
    public int testLogin(String username, String password, String validationCode, HttpServletRequest request){
       User user = userService.userLogin(username,password);
        HttpSession session = request.getSession();
        String randomCode =  (String) session.getAttribute("randomCode");
        if (user == null){
           return 1;
        }else if (!randomCode.equals(validationCode)) {
            return 2;
        }else {
           /* int uid =  user.getId();
            session.setAttribute("uid",uid);*/
            session.setAttribute("user",user);
            return 3;
        }
    }

    @RequestMapping("list")
    public List<User> getUserList(int id){
        return userService.getUserList(id);
    }

    @RequestMapping("update")
    public boolean update(User user){
        return userService.update(user);
    }

    @RequestMapping("tologout")
    public String tologout(HttpServletRequest request,HttpServletResponse response){

       Cookie[] cookies = request.getCookies();

            for (Cookie cookie : cookies) {
                     cookie.setMaxAge(0);
                     cookie.setValue("");
                     cookie.setPath("/");
                     response.addCookie(cookie);
            }
        HttpSession session = request.getSession();
        session.invalidate();
        return "index";
    }

    @RequestMapping("cookieUser")
    public int testLogin(String username, String password, HttpServletRequest request, HttpServletResponse response){


        Cookie cookie = new Cookie("username",username);
        Cookie cookie1 = new Cookie("password",password);
        cookie.setPath("/");
        cookie1.setPath("/");
        cookie.setMaxAge(60*60*24);
        cookie1.setMaxAge(60*60*24);
        response.addCookie(cookie);
        response.addCookie(cookie1);

        return 1;

    }

    //检验是否存在cookie
    @RequestMapping("testCookie")
    public void  testCookie(HttpServletRequest request,HttpServletResponse response){
        Cookie[] cookies = request.getCookies();

       String username = null;
       String password = null;
        if (cookies != null) {

            for (Cookie cookie : cookies) {

                    if ("username".equals(cookie.getName())) {
                        username = cookie.getValue();
                    }
                    if ("password".equals(cookie.getName())) {
                        password = cookie.getValue();
                    }


            }
            HttpSession session = request.getSession();
            User user = userService.userLogin(username, password);
            if (user != null) {
                session.setAttribute("user", user);
            }

        }

    }

    @RequestMapping("phone")
    public int testUserPhone(String phone){
        User user = userService.loginByPhone(phone);
        if (user != null){
            return 1;
        }else {
            return 2;
        }
    }


    @RequestMapping("updatepassword")
    public int testUpdatePassword(String phone,String password){


        return userService.updatePassword(phone,password);
    }


}
