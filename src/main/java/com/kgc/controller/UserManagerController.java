package com.kgc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.UserManager;
import com.kgc.service.UserManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("userManager")
@ResponseBody
public class UserManagerController {
    @Autowired
    private UserManagerService userManagerService;

    @RequestMapping("queryAll")
    @ResponseBody
    public PageInfo queryAll(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             @RequestParam(value = "ps", defaultValue = "10") Integer ps) {

        PageHelper.startPage(pn, ps);
        List<UserManager> detailsList = userManagerService.queryAll();

        PageInfo info = new PageInfo(detailsList, 5);

        return info;
    }

    @RequestMapping("update")
    public boolean update(UserManager userManager) {
        return userManagerService.update(userManager);
    }

    @RequestMapping("delete")
    public boolean add(int id) {
        return userManagerService.delete(id);
    }

    @RequestMapping("queryById")
    public UserManager queryById(String id) {
    return  userManagerService.queryById(id);


    }

    @RequestMapping("byid")
    public int testDetailsById(int id) {
        return userManagerService.getDetailsById(id);

    }
}
