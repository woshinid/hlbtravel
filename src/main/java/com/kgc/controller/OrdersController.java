package com.kgc.controller;

import com.kgc.entity.Orders;
import com.kgc.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@RequestMapping("orders")
@Controller
@ResponseBody
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

   /* @RequestMapping("add")
    public int testAddOrders(Orders orders){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        orders.setPublishdate(format.format(new Date()));

        return ordersService.addOrders(orders);
    }*/

    @RequestMapping("list")
    public List<Orders> testGetOrdersList(int uid){

        return ordersService.getOrdersList(uid);
    }

    @RequestMapping("delete")
    public int testDeleteOrders(String id){

        return ordersService.deleteOrders(id);
    }




}
