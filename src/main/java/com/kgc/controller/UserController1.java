package com.kgc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.User;
import com.kgc.service.UserService1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("user")
@ResponseBody
public class UserController1 {

    @Autowired
    private UserService1 userService;

  /*  @RequestMapping("login")
    @ResponseBody
    public boolean login(@RequestParam("name") String username,@RequestParam("pwd")String password, HttpServletRequest req) {
        User user = userService.queryByNameAndPassword(username, password);
        if (user != null) {
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            return true;
        }
        return false;
    }*/
    @RequestMapping("queryAll")
    @ResponseBody
    public PageInfo queryAll(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             @RequestParam(value = "ps", defaultValue = "10") Integer ps){

        PageHelper.startPage(pn,ps);
        List<User> detailsList = userService.queryAll();

        PageInfo info = new PageInfo(detailsList,5);

        return info;
    }

    @RequestMapping("update1")
    public boolean update(User user) {
        return userService.update(user);
    }

    @RequestMapping("queryById")
    public User queryById(int id) {
        return userService.queryById(id);
    }
    @RequestMapping("byid")
    public int testDetailsById(int id){
        return userService.getDetailsById(id);

    }
}
