package com.kgc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.Details;
import com.kgc.service.DetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("details")
@ResponseBody
public class DetailsController {
    @Autowired

    private DetailsService detailsService;
    @RequestMapping("queryAll")
    @ResponseBody
    public PageInfo queryAll(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             @RequestParam(value = "ps", defaultValue = "10") Integer ps){

        PageHelper.startPage(pn,ps);
        List<Details> detailsList = detailsService.queryAll();

        PageInfo info = new PageInfo(detailsList,5);

        return info;
    }
/*    @RequestMapping("queryAll")
    public List<Details> queryAll() {
        return detailsService.queryAll();
    }*/

    @RequestMapping("del")
    public boolean delete(@RequestParam("id") int id) {
        return detailsService.delete(id);
    }

    @RequestMapping("add")
    public boolean add(Details details) {
        return detailsService.add(details);
    }

    @RequestMapping("update")
    public boolean update(Details details) {
        System.out.println(detailsService.update(details));
        return detailsService.update(details);
    }

    @RequestMapping("queryById")
    public Details queryById(int id) {
        return detailsService.queryById(id);
    }
    @RequestMapping("update1")
    public boolean update1(int id){
        return detailsService.update1(id);
    }


    @RequestMapping("byid")
    public int testDetailsById(int id){
        System.out.println(detailsService.getDetailsById(id));

        return detailsService.getDetailsById(id);

    }
}

