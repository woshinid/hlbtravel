package com.kgc.controller;

import com.kgc.entity.Manager;
import com.kgc.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("manager")
public class ManagerController {
    @Autowired
    private ManagerService managerService;
    @RequestMapping("login")
    @ResponseBody
    public boolean login(@RequestParam("name") String mname, @RequestParam("pwd")String mpassword, HttpServletRequest request){
        Manager manager=managerService.query(mname,mpassword);
        if(manager!=null){
            HttpSession session=request.getSession();
            session.setAttribute("manager",manager);
            return true;
        }
        return  false;
    }
}
